---
layout: markdown_page
title: "Category Direction - Load Testing"
description: "Load testing is a common and typically important stage in the CI process for organizations of all sizes. Find more information here!"
canonical_path: "/direction/verify/load_testing/"
---

- TOC
{:toc}

## Load Testing

Load testing is a common and typically important stage in the CI process for organizations of all sizes. We will help developers [shift left](https://stackify.com/shift-left-continuous-testing/) by making performance decisions earlier in the development process by providing a measure of how software responds under load as changes are introduced. 

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALoad%20Testing)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1305) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Now that the [Load Testing MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/10683) has been delivered we will focus on broadening adoption of the tool by [adding it to AutoDevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/10681). We think this will allow more users to make use of the Load Testing feature, contribute feedback and ultimately make this better.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Add integrated load testing to AutoDevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/952).

## Competitive Landscape

### Azure DevOps

Azure DevOps offers [in-product load testing](https://docs.microsoft.com/en-us/azure/devops/test/load-test/get-started-simple-cloud-load-test?view=azure-devops).  This consists of different types of tests including:

* HTTP Archive Based tests
* URL based tests
* Apache JMeter Tests

For URL type tests, the output contains information about the average response time, user load, requests per second, failed requests and errors (if any).

### Travis CI/CircleCI

While, just as one could orchestrate any number of performance testing tools with GitLab CI today, Travis or CircleCI could be used to orchestrate performance testing tools, it does not have any built-in capabilities around this.

## Top Customer Success/Sales Issue(s)

The Field teams are typically interested in uptiered feature sets and this includes the [Premium features](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALoad%20Testing&label_name[]=GitLab%20Premium) and [Ultimate Features](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALoad%20Testing&label_name[]=GitLab%20Ultimate). The top issues for consideration in these categories are [Archive and graph load test results](https://gitlab.com/gitlab-org/gitlab/-/issues/36914) and [Add integrated load testing to AutoDevops](https://gitlab.com/gitlab-org/gitlab/-/issues/10681).

## Top Customer Issue(s)

The most popular issue in this category is to [Improve the metrics reports frontend](https://gitlab.com/gitlab-org/gitlab/-/issues/11945). The current interface for Metrics Reports is especially hard to read when the number of metrics grows beyond 10 to 12 and we hope this solves that problem.

## Top Internal Customer Issue(s)

The top internal customer request is to [integrate the Load Performance Testing template into AutoDevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/10681). We are looking forward to getting additional feedback from internal customers as usage within the company expands.

## Top Vision Item(s)
Our long term vision for Load Performance Testing is that customers trust their app is well tested to perform under load based on their real traffic trends and can see how performance is changing over time. [Graphing Load tests over time](https://gitlab.com/gitlab-org/gitlab/-/issues/36914) and [auto scaling load tests](https://gitlab.com/gitlab-org/gitlab/issues/35377)) will also move our vision for load testing forward.
