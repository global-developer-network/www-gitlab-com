---
layout: job_family_page
title: "General Ledger Accounting"
---


The General Ledger Accounting team are responsible for providing timely and accurate month end close financials, that are GAAP compliant. 

## Responsibilities
* Ensure an accurate and timely month end close by preparing various journal entries and account reconciliations for cash, prepaids, accruals, inter-company, fixed assets and various other accounts
* Coordinate with other departments to obtain transaction information, research reconciling items, and resolve issues
* Assist and or reconcile bank statements as needed
* Ensure proper accounting policies and principles are followed in accordance with GAAP
* Assist in project implementation of new procedures to enhance the workflow of the department
* Ensure compliance of internal controls as related to SOX
* Assist in quarterly reviews and annual audit with external auditors
* Support overall department goals and objectives
* Provide backup support to the accounts payable team as needed
* Provide backup support to the reporting, FPA, and operations as needed
* Assist with ad-hoc projects and management inquiries

## Requirements

* Working knowledge of GAAP principles and financial statements
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
* Must have experience with a large ERP system, Netsuite is a plus
* Proficient with excel and google sheets
* Software Company Experience preferred
* International experience preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* You share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

## Levels

### Associate General Ledger Accountant

The Associate General Ledger Accountant reports to the [Senior Manager, Accounting](https://about.gitlab.com/job-families/finance/accountant/).

#### Job Grade

The Associate Accountant is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Associate General Ledger Accountant Responsibilities

* Assist with accurate and timely month end close by preparing various journal entries and account reconciliations for cash, prepaids, accruals, inter-company, fixed assets payroll, and other various accounts. 
* Assit with proper accounting policies and principles are followed in accordance with GAAP
* Begin to learn flux analysis for month end that summarizes and tells the "story" of the activity. 
* Posting and processing journal entries to ensure all business transactions are recorded
* Understand the activity for 1 of the 5 areas of the company organization (Marketing, G&A, R&D, Sales, COS) and the related month end accounting processes.
* Assist with compliance of Sarbanes-Oxley Section 404 key controls in the financial areas of responsibility, as applicable.
* Assist with reviewing of expenses, payroll records etc. as assigned
* Reconciles the general ledger accounts monthly
* Work with and support the accounting team in day-to-day activities, special projects, and workflow process improvements.
* Assist senior accountants in the preparation of monthly/yearly closings
* Assist with other accounting projects

#### Associate General Ledger Accountant Requirements

* 1-3 years experience public accounting experience preferred, or working toward a CPA
* Proficient with Microsoft Office suite and/or Google Docs and Sheets
* Experience working with international subsidiaries, including statutory reporting requirements, strongly preferred
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* You share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

### General Ledger Accountant (Intermediate)

The General Ledger Accountant (Intermediate) reports to the [Senior Manager, Accounting](https://about.gitlab.com/job-families/finance/accountant/).

#### Job Grade

The Intermediate Accountant is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### General Ledger Accountant (Intermediate) Responsibilities

* Extends the Associate General Ledger Accountant responsibilities
Ensure proper accounting policies and principles are followed in accordance with GAAP
* Understand how to perform a flux analysis for month end that summarizes and tells the "story" of the activity. 
* Understand the activity for 2 of the 5 areas of the company organization (Marketing, G&A, R&D, Sales, COS) and the related month end accounting processes.
* Ensure compliance of Sarbanes-Oxley Section 404 key controls in the financial areas of responsibility, as applicable.
* Assist with reviewing of expenses, payroll records etc. as assigned
* Provid account detail and support and explain variances to internal stakeholders.
* Work with and support the accounting team in day-to-day activities, special projects, and workflow process improvements
* Support overall department goals and objectives.
* Work with operations and other areas to ensure accruals are properly captured
* Assist with other accounting projects
* This position reports to the Sr. Accounting Manager.

#### General Ledger Accountant (Intermediate) Requirements

* Extends the Associate General Ledger Accountant requirements
* 3-5 years experience public accounting experience preferred, or working toward a CPA

### Senior General Ledger Accountant

The Senior General Ledger Accountant reports to the [Senior Manager, Accounting](https://about.gitlab.com/job-families/finance/accountant/).

#### Job Grade

The Senior Accountant is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Senior General Ledger Accountant Responsibilities

* Extends the General Ledger Accountant (Intermediate) responsibilities
* Perform flux analysis for month end that summarizes and tells the "story" of the activity.
* Create a Standard Operating Procedure (SOP) manual for accounting policy and regulations, and roll out reconciliation procedures.
* Guide/train associate accountants and other staff by answering questions and reviewing their work
* Proficient with accruals and understanding their impact and can assist others.
* Understand the activity for 3 or more of the 5 areas of the company organization (Marketing, G&A, R&D, Sales, COS) and the related month end accounting processes.
* Assist with monitoring the need for business process improvements and assist with the design processes, procedures, and reporting enhancements to improve financial and operational processes.
* Support the design and implementation of new policies and procedures related to audit requirements and business activities.
* Play a vital part in the company's financial management which includes account reconciliations and reporting to necessary parties.
* Part of the development and implementation of systems related to the accounting team. 
* Work with and support the accounting team in day-to-day activities, special projects, and workflow process improvements.
* Support overall department goals and objectives.
* Producing error-free accounting reports and present their results
* Analyzing financial information and summarizing financial status
* Spot errors and suggest ways to improve efficiency and spending

#### Senior General Ledger Accountant Requirements

* Extends the Associate General Ledger Accountant requirements
* 5-7 years experience public accounting experience preferred, or working toward a CPA

### Manager, Accounting 

The Manager, Accounting reports to the [Senior Manager, Accounting](https://about.gitlab.com/job-families/finance/accountant/).

#### Job Grade

The Manager, Accounting is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Accounting Responsibilities

* Manage a timely and accurate month end close by reviewing various journal entries and account reconciliations for cash, prepaids, accruals, inter-company, fixed assets payroll, and other various accounts. 
* Review and/or prepare flux analysis for month end that summarizes and tells the "story" of the activity.
* Review/Create Standard Operating Procedure (SOP) manual for accounting policy and regulations, and roll out reconciliation procedures.
* Guide/train GL accountants by answering questions and reviewing their work
* Proficient with accruals and understanding their impact and can assist others
* Understand/Perform the activity of all 5 areas of the company organization within 6 months of starting the position (Marketing, G&A, R&D, Sales, COS) and the related month end accounting processes.
* Ensure compliance with Sarbanes-Oxley Section 404 key controls in the financial areas of responsibility, as applicable, and work with IA to update key controls as neccessary
* Monitor the need for business process improvements and assist with the design processes, procedures, and reporting enhancements to improve financial and operational processes.
* Support the processing of misc cash receipts.
* Support the design and implementation of new policies and procedures related to audit requirements and business activities.
* Play a vital part in the company's financial management which includes account reconciliations and reporting to necessary parties.
* Part of the development and implementation of systems related to the accounting team. 
* Work with and support the accounting team in day-to-day activities, special projects, and workflow process improvements.
* Support overall department goals and objectives.
* Producing error-free accounting reports and present their results
* Analyzing financial information and summarizing financial status
* Spot errors and suggest ways to improve efficiency and spending

#### Manager, Accounting Requirements

* 8-9 years experience with 2-3 years in public accounting experience
* Have or actively persuing CPA

## Performance Indicators

- [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
- [Number of material audit adjustments = 0](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)
- [Percentage of ineffective Sox Controls = 0%](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Career Ladder

The next step in the General Ledger Accounting job family is to move to the [Controller](job-families/finance/corporate-controller) job family.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidate will have a 25 minute interview with the Hiring Manager, 
* Next, candidates will have separate 25 minute interview with 2 or 3 peers,
* Finally, candidates will have a 50 minute interview with a member of the Executive team or the next level manager.

See more details about our interview process [here](/handbook/hiring/interviewing/).
