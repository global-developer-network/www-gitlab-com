---
layout: handbook-page-toc
title: "Professional Services Project Management"
---

## On this page


GitLab takes our customer's success very seriously. In Professional Services (PS) we strive to provide a first class experience for all engagements.

![](./image.png)

[Source](https://docs.google.com/spreadsheets/d/1uRoB73lZtvNhkk-Z9eQS3_Ys4yX8vVUXXqQUO7Nm1ss/edit?usp=sharing)
[Full Process Flow] (https://docs.google.com/spreadsheets/d/1Y8wWj5g8T6HuPGEnHaWUfdtI7LPmathGaMwfiA2b_Ks/edit?usp=sharing)
 
### Project plan
 
Each PS Engagement will include a Project Plan based on the activities outlined in the Statement of Work (SOW) and/or other discussions between the GitLab Professional Services and the customer.  The Project Plan may include a Gantt chart, tasks list via spreadsheet, or another form of documented plan. This plan will follow the process listed above.
 
Each PS engagement will have a Google Sheet that shows the contracted revenue and the estimated cost measured in hours or days of effort required to complete the engagement. The cost estimate must be completed prior to SOW signature and attached to the opportunity in SFDC.
 
### Project workflow
 
1. Sr. PS Project Coordinator: Once a project has moved for Closed/Won in Salesforce, a PS Object is created in Salesforce and a PS project/issues is created in GitLab.  Then, a project owner is assigned; this can be Project Manager, or Professional Services Engineer (PSE).
 
1. Sr. PS Project Coordinator: Send [initiation email](https://docs.google.com/document/d/1je9dqVJpFFMv7aw9WhPeQ8aufx6Sj3OZveqaHHd212w/edit) and [existing customer initiation email](https://docs.google.com/document/d/1eNPXLmstMLoatpOBIhxrJgnPFgqYByPaJoFQRd2kz9U/edit).
 
1. Project Owner: Begins project with the following high-level process:
   - Internal Sales to PS hand-off meeting
 
   - External Customer Project Kickoff: Use this [Kick-off slide template](https://docs.google.com/presentation/d/1HtVIE64N94Rcc774ujllClGmYZ5y1_ApE4-O3pazR6k/edit#slide=id.g59bfc474c5_2_145) and this [agenda/meeting template](https://docs.google.com/document/d/1WPnBQUOT2dug8rPkA-VFzXtE1AlQJGMMROQPhHOh4Bg/edit). Alternatively, the actions and meeting minutes can be added directly to the [Project Definition Template](https://docs.google.com/spreadsheets/d/1t_vVPvzh0aGBBB-5tCcl8e7Gi_CbJ6hXGuRxSTyZ9fQ/edit?usp=sharing).
   - Weekly project meeting (if applicable): Use [this template](https://docs.google.com/document/d/1WPnBQUOT2dug8rPkA-VFzXtE1AlQJGMMROQPhHOh4Bg/edit)
   - Provide weekly Executive Status Report: Use [this template](https://docs.google.com/document/d/1tPsQbaq36zs4oKh6LXKXQPGy4Dmk7gbQfiZN4duu81o/edit)
   - Change request (required for scope changes) use [this template] (https://docs.google.com/document/d/1zed5AsEpjzwII0HaIjsmXYaRAp5qHY-BGJfVCISVGcM/edit?usp=sharing)
   - Project closure meeting: Use [this agenda template](https://docs.google.com/document/d/1Cw5eLe8VKFtHG9xGqUiCua8Pbu52reMzHujcPWq3ofQ/edit)
  
1. PS Project Manager or PS Project Coordinator: Complete [this sign off workflow](/handbook/customer-success/professional-services-engineering/workflows/project_execution/sign-off.html)
 
1. PS Project Manager or PS Project Coordinator: Complete this [financial wrap-up workflow](/handbook/customer-success/professional-services-engineering/workflows/internal/financial-wrapup.html)
