---
layout: handbook-page-toc
title: SA Practices
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

## SA Practices

Solution Architects have various practices:

[Communities of Practice](/handbook/customer-success/solutions-architects/sa-practices/communities-of-practice)

[Deliberate Practice](/handbook/customer-success/solutions-architects/sa-practices/deliberate-practice)

**Recognizing Cognitive Bias**

What is Cognitive Bias:

- <span class="colour" style="color:rgb(0, 0, 0)">Perception that occurs when people are processing and interpreting information in the world around them</span>
- <span class="colour" style="color:rgb(0, 0, 0)">Cognitive biases are often a result of your brain's attempt to simplify information processing</span>
- <span class="colour" style="color:rgb(0, 0, 0)">Our blindspots characterized by unconscious bias</span>
- <span class="colour" style="color:rgb(0, 0, 0)">It affects the decisions and judgments that they make</span>
- <span class="colour" style="color:rgb(0, 0, 0)">Common misconceptions about people: looks, personalities, mental models</span>

Please read this [list](https://www.verywellmind.com/cognitive-biases-distort-thinking-2794763) to understand the 10 most common types of cognitive biases.

How to facilitate Cognitive Bias exercise:

1. Create a survey to collect information & feedback from team members about their own bias. This can be potentially recognized by reading the Top 10 most common cognitive biases above. Feel free to use [this](https://docs.google.com/forms/d/e/1FAIpQLSczPQ8GhjRfLTjnUBj-Oio1JfUb5J4BqjzLKyQTYTaXv7vwiw/viewform).
1. In a small team meeting setting:
a. Summarize the results of the survey
b. Pick 2-3 examples for a role-playing exercise
c. Discuss the results & exercise
Example of the exercise [here](https://docs.google.com/presentation/d/1yGeNl-dGUFZM2I9RY-3J7ZdslaGw1Vu_xSSm0r2qHH8/edit?usp=sharing)

[TODO] Effective Objection Handling