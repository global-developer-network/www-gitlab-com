To assign weights to issues in a future milestone, we ask two team members to
take the lead each month. They can still ask questions - of each other, of the
rest of the team, of the stable counterparts, or anyone else - but they are the
initial.

To weight issues, they should:

1. Look through issues with the ~"workflow::planning breakdown" 
   label, filtered by your group and ~"Next Up" (you can use the links in the table below).
2. For those they understand, they add a weight. If possible, they also add a
   short comment explaining why they added that weight, what parts of the code
   they think this would involve, and any risks or edge cases we'll need to
   consider.
3. Timebox the issue weighting overall, and for each issue. The process is
   intended to be lightweight. If something isn't clear what weight it is, they
   should ask for clarification on the scope of the issue.
4. If two people disagree on the weight of an issue, even after explaining their
   perceptions of the scope, we use the higher weight.
5. Start adding weights around a week before the weights for a milestone
   are due. Finishing earlier is better than finishing later.


