---
layout: handbook-page-toc
title: "Sales Kickoff"
description: "The GitLab Sales Kickoff sets the tone for the GitLab field organization for the new fiscal year"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Kickoff (SKO) Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

# Sales Kickoff 2021
SKO 2021 is scheduled to take place virtually during the week of Feb 8-11, 2021. Our theme is Peak Performance. Check out our [SKO 2021 Theme Reveal Video](https://vimeo.com/487653102) for a preview of the brand design. 

More details coming soon!

## High-level Sales Kickoff 2021 Agenda 
Virtual SKO 2021 will consist of half-day sessions each morning (PT) during the week of Feb 8-11, 2021. We will offer live content in two time zones to accommodate our team members around the world. Below is a high-level agenda for each time zone: 

### AMER/EMEA Time Zone
*  Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO. 
*  Mon, 2021-02-08: Keynotes, Customer Speaker, Q&As, regional team building (AMER)
*  Tues, 2021-02-09: Keynotes, Q&As, regional team building (EMEA)
*  Wed, 2021-02-10: Awards ceremony, role-based breakout sessions, Partner Summit
*  Thurs, 2021-02-11: Role-based breakout sessions, Comp Plan information session 

### APAC Time Zone (Based on AEDT)
*  Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO.
*  Tues, 2021-02-09: Keynotes, Customer Speaker, Q&As, regional team building
*  Wed, 2021-02-10: Keynotes, Q&As, regional team building (EMEA)
*  Thurs, 2021-02-11: Awards ceremony, role-based breakout sessions, Partner Summit
*  Fri, 2021-02-12: Role-based breakout sessions, Comp Plan information session 

** Note that agenda details are subject to change. 

## SKO 2021 Tickets 
You must book a ticket for this event. This is how we keep track of who is attending and all other details. Please look out for registration details via email and Slack in January 2021. Registration should be completed no later than 2021-02-05. 

We will  validate all registrations on the back end, and any unapproved registrants will be canceled. Please do not book if you are not approved to attend this event. See the FAQs below for more information about who is approved to attend SKO. 

**Ticket Types:**
- GitLab Team Member Attendees - GitLab Internal Team Members (Sales, Customer Success, Channel, Alliances, SDR and supporting teams) 
- Channel Partner Ticket - External channel partners joining GitLab Sales Kickoff

## Sales Kickoff 2021 FAQ 
**Q: What are the dates and location?**

A: Sales Kickoff (SKO) will take place virtually from Feb 8-11, 2021. 

**Q: Why are we having a virtual event versus an in-person SKO?**

A: The health and safety of our team is our top priority and a remote event allows us to keep planning with confidence while there is still uncertaintly about when and how to host large in-person events.

**Q: Will we still have an in-person Sales event in 2021?** 

A: We hope to! Sales leadership is looking into opportunities to get the team together in person either by region or with the entire team in 2H FY22. This is contingent on the status of the COVID-19 pandemic, and we will make all decisions with the health and safety of our team members as our top priority.

**Q: What platform(s) are we using for the virtual event?**

A: We are using Hopin as our virtual event platform. This is the same platform we used for Contribute. 

**Q: When and how should I register?**

A: The SKO core team has reached out via email with registration links. Please complete registration through this email by 2021-02-05.

**Q: Who is approved to attend SKO?**

A: SKO attendees are set and approved by Sales and Marketing leadership. Attendees include all Field Team members (Sales, Customer Success, Channel, Alliances, SDR), select Marketing attendees appointed by the CMO, and other supporting team members from People Ops, Legal and Product. 

If you believe you should be included in SKO based on the categories outlined above, please reach out to David Somers (for Field and other supporting teams) or Todd Barr/Marketing leadership (for Marketing team members).

**Q: How are we accommodating different time zones?**

A: We will have live and interactive sessions for all time zones. All sessions will be recorded and available for team members on-demand after the event.

**Q: Which time zone should I attend?**

A: You will be added to the registration for the time zone that coordinates with the country where you are based – AMER/EMEA or APAC. If you'd like to view the Save the Date invites for the alternate time zone, you can do so on the [Field Enablement Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV81bjNnNjBsNTh0aHVtOWFvdnA4aWlzYXYzNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) (navigate to the week of February 8, 2021).

**Q: Will there be any pre-work?**

A: In support of GitLab’s all-remote culture, all SKO participants are  asked to complete pre-work to help you get the most out of the planned content and make the best use of our time together.

To access the course, go to gitlab.edcast.com, sign in via SSO, click on "My Learning Plan," then click on the "SKO 2021 Pre-Work" course. The pre-work will take approximately 2-2.5 hours to complete, so please plan accordingly.

**Q: What will I be expected to participate in?**

A: SKO is a great opportunity for our team to come together to both celebrate our results and collaborate for a strong start in the new year. As such, the agenda is purposely crafted with numerous activities to learn, network and celebrate together. We expect that every SKO to treat it as a “work trip” and block your calendar to ensure you fully participate in every part of the event.

**Q: I just got hired – how do I attend SKO?**

A: All eligible SKO attendees have until 2021-02-05 to register for the event. If you are a new hire and you have not received a registration email, please Slack #sko-virtual-2021. 

**Q: How will we make SKO 2021 exciting even though it's virtual?**

A: The SKO core team is preparing an engaging virtual event. Everything from the virtual platform to the agenda to the event theme is being carefully curated to ensure that our team feels connected and energized while participating in SKO. 

**Q: Will we still have an Awards Dinner and networking time?**

A: Rather than an Awards Dinner, we will host an Awards Ceremony on Wednesday. The SKO core team is planning fun and creative ways to ensure 2021 Sales Awards winners are celebrated. We have also built networking time into the agenda each day to give team members time to connect across the organization and with partners.

**Q: How will we include partners in SKO 2021?**

A: We will host a Partner Summit in tandem with SKO. Partners will be invited to participate in our Day 1 keynotes on Tuesday, and they will have their own Partner Summit-specific sessions on Wednesday. Partners will be available to virtually meet and greet with GitLab team members in the virtual Partner Booths each day of SKO after the half-day keynotes or breakout sessions have concluded.

**Q: Where can I ask questions related to SKO?**

A: If your question is not covered in the FAQ, please feel free to post in the [feedback issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/535). Alternatively, you can visit the public slack channel #sko-virtual-2021 and ask your question there. If your question is not-public, you can email sales-events at gitlab dot com with questions. Members of the core SKO planning team, including Emily Kyle, David Somers and Monica Jacob are actively monitoring this email and will respond to your inquiry within 24 hours. 

We are actively planning this event and will update this FAQ as more information becomes available. 

Reference: [Company Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)

----

# Sales Kickoff 2020
In February 2020, the entire sales organization and their supporting teams gathered in Vancouver, BC Canada for the inaugural GitLab Sales Kickoff. This page includes slide decks, videos, and pictures from the event.

## SKO 2020 Theme
**Level Up** - It works across nationalities / languages and has lots of ways we can tie it into the content. All about leveling up the business.

## SKO 2020 Day 1 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Welcome & FY21 Overall GitLab Strategy & Vision | [slides](https://drive.google.com/open?id=1mvqXfa3vIQec8mhBSzVB5gZDC7LJwdMhjHev0nkVj9g) (GitLab internal) | [video](https://youtu.be/kc3H0gMC7u4) (GitLab internal) |
| Product Strategy & Vision with Q&A | [slides](https://drive.google.com/open?id=1DfsjznCwqBz9MpueftriC1QS81QHuXcdAACqtdcFPU0) (public) | [video](https://youtu.be/fS5T-XOvXPk) (public) |
| FY21 Sales & Marketing Strategy & Vision with Q&A | [slides](https://drive.google.com/open?id=1bq_bxM07PTNF3rfAbFB_Fia7U8CszfLTEJUrF-bfilI) (GitLab internal) | [video](https://youtu.be/8vrp1AGRp1U) (GitLab internal) |
| CRO Staff Level Up Panel Discussion | no slides | [video](https://youtu.be/EYlC9uP3LoE) (GitLab internal) |
| Leveling Up with Partners at GitLab | [slides](https://drive.google.com/open?id=12j219pElrox1hUyMpiOwSSrTV6S6biLurahKuLjvH1Y) (GitLab internal) | [video](https://youtu.be/jMUzgPIfFXg) (GitLab internal) |
| Keys to Winning panel discussion | [slides](https://drive.google.com/open?id=1V1lDVIJyX1mMin2Hm7AaExps0WAgczi6vAB6UWFuR8c) (GitLab internal) | [video](https://youtu.be/ervvabavL2o) (GitLab internal) |
| Sales Kickoff Awards Ceremony | [slides](https://drive.google.com/open?id=1deR4D2GplTGan1E2ENZbhZ0rt4YiUzZEJGX7RLj83yY) (GitLab internal) | [video](https://drive.google.com/open?id=1_lbLGvYhhB6ynyaWngeuE6nkDUcUXv1A) (GitLab internal) |


## SKO 2020 Day 2 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Day 2 Welcome | no slides | [video](https://youtu.be/bI1XBeZajIs) (GitLab internal) |
| Articulating and Selling the GitLab Vision & Customer Journey | [slides](https://drive.google.com/open?id=14kO1iTqSwuvV-7CDHThslUv8_QP_EnaZg4yG-1romZc) (GitLab internal) | [video](https://youtu.be/DtL38mgpycE) (GitLab internal) |
| Getting Into Accounts That Say They Don’t Have a Problem | [slides](https://drive.google.com/open?id=1dRdKs7BkbyTzTfFZ4JwwjJUIwgTSSOjkG-DEGDGgN30) (public) | N/A (not recorded) |
| Proactively Competing Against Microsoft | [slides](https://drive.google.com/open?id=1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8) (GitLab internal) | [video](https://youtu.be/Ds_U8iiUOz8) (GitLab internal) |
| Customer Expectations and Storytelling | [slides](https://drive.google.com/open?id=1hnYT7ulTPpb7C3V_fKdOU9gajAA1oufzp9EiDYQC-mU) (public) | N/A (not recorded) |
| Finding and Defining the Customer’s Problem | [slides](https://drive.google.com/open?id=18NynGuJTEwSUnKkRR4A1fQa2xojPxpO3mlnvb3BmMXY) (public) | [video](https://youtu.be/_RSqUgYDjdQ) (GitLab internal) |
| One Lab, One Story: Leveling Up Our Demo Labs | [slides](https://drive.google.com/open?id=1x125pYEjAQQX5vDo5u2eanAoVtoz5R2muiaYqXL_xEE) (public) | N/A (not recorded) |
| Customer Success Plan Workshop | [slides](https://drive.google.com/open?id=15Qt-UcfRt9cX-4CV7zMsurojTZg_8Kf-u0dMYL16JXQ) (public) | N/A (not recorded) |
| Gaining Access and Pitching to the Economic Buyer | [slides](https://drive.google.com/open?id=166GA0LyvQLG6y-9qAuKoJ6iZUxmjpjklLSZ1J9wC6z0) (public) | N/A (not recorded) |
| Command Plan Excellence | [slides](https://drive.google.com/open?id=1Vjn5ICOpwxbZNFRZhvpwZc0REkHMeEKNSakpFV04EuM) (public) | [video](https://youtu.be/zN_0J6syxmM) (GitLab internal) |
| Technically Competing Against MSFT in CI Use Case | [slides](https://drive.google.com/open?id=1Rfsk5_h5O6DF14wjgmd7WrzQPCC1yGzYPPdYG_JVApg) (GitLab internal) | N/A (not recorded) |
| Next Level Engagements: Consulting Acumen | [slides](https://drive.google.com/open?id=1ahDNo93BpNSRonLj6C3iWKZfeJy1ZfJZosujpzSzo2U) (public) | N/A (not recorded) |
| Proactively Competing Against Jenkins | [slides](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg) (GitLab internal) | N/A (not recorded) |
| Account Planning | [slides](https://drive.google.com/open?id=1XPhv3zqtR-q5sKgDx1HhnNkaQUSIVTO0pXj2wPdjafs) (public) | [video](https://youtu.be/-vPkzlWLZ8o) (GitLab internal) |
| From SCM and CI to Security: Paths to Ultimate | [slides](https://docs.google.com/presentation/d/1VTCfKcAZ4NS2xQu8C-fA6pugCdvUbb0MfQhimLiAGak/) (public) | N/A (not recorded) |
| Next Level Engagements: Consulting Methodology | [slides](https://drive.google.com/open?id=1TWg_grUCegOnJK8yCXxq7lRgVCByoJMCd59u3A1QjMo) (public) | N/A (not recorded) |


## SKO 2020 Wrap Up
* [SKO Wrap Up presentation](https://docs.google.com/presentation/d/1MwJRWCGl-U2qic_h3xHQxGDCUf1s0R23aKEIrXqcXW0/edit?usp=sharing) (public)
* [GitLab FY21 Sales Kickoff Wrap Up and Survey Results](https://youtu.be/_q9M9_nwNy4) video (public) 

## GitLab Infomercial
* [Public version on YouTube](https://youtu.be/gzYTZhJlHoI) featuring David Astor
* [Long version](https://drive.google.com/open?id=1yhG_JLh4rpayRvJYkYE4EJYMcNZkrUXd) (GitLab internal)

## SKO 2020 Pictures
* [Sales Kickoff photo album](https://photos.app.goo.gl/hcvEyzH3wDdccrQw7)
* [LinkedIn headshots](https://drive.google.com/open?id=1_Laql3qZBj9hp6CXrHCs7ovrCQOGR1gx) 
